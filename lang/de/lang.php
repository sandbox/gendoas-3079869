<?php
/**
 * german language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author      Albin Spreizer <albin@gendoas.de>
 */
$lang['no_nesting']   		= '~~Accordions können nicht geschachtelt werden~~';
$lang['head_caption']   	= 'Abschnitt';
$lang['js']['juiacctest']	= 'Dies ist eine JS-Testvariable'; 
