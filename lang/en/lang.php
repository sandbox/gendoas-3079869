<?php
/**
 * german language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author      Albin Spreizer <albin@gendoas.de>
 */
$lang['no_nesting']         = '~~nested accordions not supported~~';
$lang['head_caption']   	= 'Section';
$lang['js']['juiacctest']	= 'This is a js-testvariable'; 
