    //
$jui = $jui || {};
$jui.juiacc = {
    onJqReady: function(data) {
        if ( !juiwindata.widgetsExists.juiacc ) {
            return;     // no accordions on that page
        }

        juiwindata.oninit = true;
        delete juiwindata.waiting.juiacc;
        var accs = $jq('.juiacc-widget', data['context']);
        accs.each(function() {
            try {
                var acc = $jq(this);
                if (acc.data('ready') || $jui.widget.getClosestNotReady(acc)) return;

                var vars = acc.data('juivars') || {}
                ,   juiflags = acc.data('juiflags') || {}
                ,   state   = vars.state || {}
                ,   jopts   = vars.juiacc || {}
                ,   oopts   = widgetOpts(jopts.opts)
                ,   fillspace = juiflags.fillspace || false
                ,   keep_active = typeof oopts.active === 'number' ? oopts.active : -1
                ,   vresizer  = vars.resizer || {}
                ,   rflags    = vresizer.addFlags || {}
                ;
                if (juiflags.nostorestate) _clearState(state);
                if (rflags.nostorestate && state.resizer) delete state.resizer;
                vars.state = state;
                
                oopts.active =  -1;    // no selection frst
                // *********************************************
                // create accordion-object
                acc.accordion(oopts).data('self', acc);
                // *********************************************
                if (keep_active < 0 && !oopts.collapsible) keep_active = 0;
                oopts.active = keep_active;  // restore
                if (juiflags.nostorestate) state.active = false;
                state.active = typeof state.active === 'number'
                                ? state.active : keep_active;

                var dacc = acc.data('accordion')
                ,   headers  = dacc.headers
                ,   options  = dacc.options
                ,   panels   = $jq('.juiacc-content', acc)
                ,   wfuncs   = widgetFuncs()
                ,   closest  = $jui.widget.getClosest(acc)
                ,   closestContent  = closest ? $jui.widget.getClosestContent(acc) : false
                ,   resizable = fillspace ? false : juiflags.resizable || false
                ,   style   = jopts.style || false
                ,   size    = $jui.resizer.getSize(state.resizer, style, fillspace)
                ,   height  = size.height.value || false
                ,   width   = size.width.value  || false
                ;
                if (closest) closest = closest.data('self');
                acc.extend({plugin:     vars.plugin,
                            headers:    headers,
                            options:    options,
                            panels:     panels,
                            juiwidget:  acc,
                            juistate:   state,
                            juivars:    vars,
                            juiwfuncs:  wfuncs,
                            juiclosest: closest,
                            juiflags:   juiflags,
                            rflags:     rflags
                });

                // add/remove class, attr, style
                // for widget, header and panels
                var head      = vars.head || {}
                ,   content   = vars.content || {}
                ,   sections  = vars.sections || {}
                ;
                $jui.widget.setAttrCss(acc, jopts);
                $jui.widget.setAttrCss(headers, head);
                $jui.widget.setAttrCss(panels, content);
                $jui.widget.setSectsAttrCss(headers, sections, 'head');
                $jui.widget.setSectsAttrCss(panels, sections, 'content');

                
                height = fillspace ? 1 : $jui.resizer.sizeExists(height) ? height : false;
                width  = fillspace ? 1 : $jui.resizer.sizeExists(width)  ? width  : false;

                var hasResizer = fillspace || resizable || height || width;
                if (hasResizer) {
                    acc.accordion('option', 'autoHeight', true);
                    acc.extend({hasResizer:true});
                    height = height || 1;
                    width  = width  || 1;
                    var ropts = resizable ? vars.resizer || {} 
                                          : {opts: {disabled:true, handles:'none'}}
                    ,   orszr = resizerOpts(ropts.opts)
                    ,   funcs = resizerFuncs()
                    ,   xrszr = $jui.resizer.getExtends(size, funcs)
                    ;
                    acc.resizable(orszr).extend(xrszr).juiAddChildren();
                    acc.extend({juiresizable: acc.data('resizable')});
                }

                // select active
                acc.accordion("activate", state.active);

                if (hasResizer) {
                    panels.css('overflow', 'auto');
                    if (fillspace && closestContent) {
                        closestContent.addClass('jui-fillspace');
                    }
                    acc.juiResize();
                    var action = !resizable 
                                    ? 'disable' 
                                    : state.active > -1 ? 'enable' : 'disable';
                    acc.resizable(action);
                }

                acc.data('ready', true);
            }catch(e){ $jui.dbgAlert('juiaccOnJqReady.each()', e.message); }
            
            function _clearState(state) {
                if (state.active) delete state.active;
            }
        });
        juiwindata.oninit = false;

        //
        // functions
        //
        function widgetOpts( opts ) {
            opts = opts || {};
            $jq.extend(opts, {
                changestart: function (event, ui) {
                    var acc = $jui.widget.getSelf(this)
                    ,   state = acc.juistate
                    ,   juiflags = acc.juiflags
                    ;
                    acc.curContent = ui.newContent;
                    if (!juiwindata.oninit && state && !juiflags.nostorestate) {
                        state.active = acc.find(ui.options.header).index(ui.newHeader);
                        $jui.ajax.storeState($jui.widget.getCleanID(this.id), state)
                    }

                    // may be ajax call is needed
                    $jui.ajax.parseContent(this, ui.newContent);
                },
                change: function(event, ui) {
                    var acc = $jui.widget.getSelf(this)
                            ,   state = acc.juistate
                    if (acc.hasResizer) {
                        if (state.active == -1) {
                            var style  = {height: getHeadersHeight(acc) + 5};
                            $jui.style.important(acc, style, true);
                            $jui.sbtoggler.resize();
                        } else {
                            acc.juiResize();
                        }
                        if (acc.juiflags.resizable) {
                            acc.resizable(ui.newHeader[0] ? 'enable' : 'disable');
                        }
                    }
                }
            });
            return opts;
        }

        function resizerOpts( opts ) {
            opts = opts || {};
            $jq.extend(opts, {
                disabled: true,
                start: function(event, ui) {
                    juiwindata.JuiResizing = true;
                },
                resize: function(event, ui){
                    var acc = $jui.widget.getSelf(this)
                    ,   juisize  = acc.juisize
                    ,   panels   = acc.panels
                    ,   dy  = ui.size.height - juisize.panelheight
                                             - getHeadersHeight(acc)
                                             - parseFloat(panels.css('padding-top'))
                                             - parseFloat(panels.css('padding-bottom'))
                    ;
                    juisize.panelheight += dy;
                    panels.css('height', juisize.panelheight);
                    return false;
                },
                stop: function(event, ui) {
                    var acc = $jui.widget.getSelf(this);
                    if (!juiwindata.oninit) {
                        var height  = ui.size.height
                        ,   width   = ui.size.width
                        ,   juisize = acc.juisize
                            ,   state = acc.juistate
                        ;
                        state.resizer = juisize.calcSizeVal(height, width, juisize);
                        acc.juiResize();
                        if (!acc.rflags.nostorestate) {
                            $jui.ajax.storeState($jui.widget.getCleanID(this.id), state)
                        }
                    } else {
                        $jui.dbgAlert('acc.resizerOpts.stop', 'juiwindata.oninit !!!');
                    }
                    juiwindata.JuiResizing = false;
                }
            });
            return opts;
        }

       function widgetFuncs () {
            return {
                tbwrenchClick: function(data) {
                    try {
                        var wgt = data.widget
                        ,   id = wgt[0].id
                        ,   idx = wgt.juistate.active
                        ,   header = wgt.headers[idx] || {}
                        ,   size   = wgt.juisize
                        ,   hpx = (size.height.valuePx).toFixed(0)
                        ,   hpc = size.height.valuePc
                        ,   wpx = (size.width.valuePx).toFixed(0)
                        ,   wpc = size.width.valuePc
                        ,   fs  = wgt.juiflags.fillspace || false
                        ,   rszr = wgt.juiflags.resizable || false
                        ,   coll = wgt.options.collapsible
                        ,   titel  = $jq.trim($jq(header).text()) || 'none'
                        ,   pheight = size.panelheight
                        ,   pwgt = wgt.juiclosest
                        ,   parents = pwgt ? '' : 'none'
                        ,   cwgt = wgt.juiChildren
                        ,   children = cwgt ? '' : 'none'
                        ,   cid = wgt.curContent[0].id
                        ,   ajaxsource = wgt.ajax && wgt.ajax[cid] 
                                            ? wgt.ajax[cid].source || 'none' 
                                            : false
                        ,   ajaxpageid = ajaxsource ? wgt.ajax[cid].pageid || false : false
                        ,   ajaxparsed = ajaxsource ? wgt.ajax[cid].parsed || {} : {}
                        ;
                        if (rszr) {
                            var h = wgt.juiresizable.options.handles;
                            rszr += ' (' + h + ')';
                        }

                        while (pwgt) {
                            parents += "\n    -> " + pwgt[0].id;
                            pwgt = pwgt.juiclosest;
                        }
                        for (var cid in cwgt) {
                            children += "\n    -> " + cid
                        }
        // collapsible
                        var msg = "ToDo:: Weitere Funktionalität\n"
                                + "(Menü/Tools/Einstellungen/Filter, ...)\n"
                                + "\nwidget = " + id
                                + "\nheight = " + hpx + " / " + hpc
                                + "\nwidth = " + wpx + " / " + wpc
                                + "\nfillspace = " + fs
                                + "\nresizable = " + rszr
                                + "\ncollapsible = " + coll
                                + "\n\nactive = " + titel
                                + "\nid = " + header.id
                                + "\nheight = " + pheight
                                + "\n\nparents: " + parents
                                + "\nchildren: " + children
                                + "\npage: " + wgt.juivars.page
//                                + "\nnamespace: " + NS
                                + "\najax.source: " + ajaxsource
                            ;
                        
                        return {msg:msg, ajaxpageid:ajaxpageid};
                        
//                        if (ajaxparsed && ajaxparsed.page) {
//                            msg += "\n\nSeite bearbeiten: \n   " + ajaxparsed.page;
//                            if (confirm(msg) !== false) {
//                                var href = DOKU_BASE + 'doku.php?id=' 
//                                            + ajaxparsed.page + '&do=edit&rev=';
//                                window.open(href, '_blank');
//                            }
//                            
//                        } else {
//                            alert(msg);
//                        }
                    }catch(e){ $jui.dbgAlert('tbwrenchClick()', e.message); }
                }
            }
        }

        function resizerFuncs () {
            return {
                readypx: function(data) {
                    var acc = data.rszr     // rszr===acc
                    ,   spx = data.sizepx
                    ,   accMin = getHeadersHeight(acc) + 90
                    ,   state = acc.juistate || {}
                    ,   active = state.active
                    ,   height = (active == -1)
                                    ? getHeadersHeight(acc) + 5
                                    : parseInt(Math.max(spx.height, accMin))
                    ;
                    return {height: height, width:  spx.width};
                },
                ready: function(data) {
                    var acc = data.rszr     // rszr===acc
                    ,   panels    = acc.panels
                    ,   juisize   = acc.juisize
                    ,   height    = parseInt(acc.height()
                                          - getHeadersHeight(acc)
                                          - parseFloat(panels.css('padding-top'))
                                          - parseFloat(panels.css('padding-bottom'))
                                    )
                    ;

                    panels.css({height: height});
                    juisize.panelheight = height;
                }
            }
        }

        function getHeadersHeight(acc) {
            var hs = acc.headers || acc.data('accordion').headers;
            return hs.length > 0 ? hs.length * $jq(hs[0]).outerHeight(true) : 0;
        }

    }

};

