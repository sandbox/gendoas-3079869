<?php
if (!defined('DOKU_INC')) die('');
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
if (!defined('DOKU_PLUGIN_JUI')) define('DOKU_PLUGIN_JUI',DOKU_PLUGIN.'juiwidget/');
if (!defined('NL')) define('NL',"\n");


require_once DOKU_PLUGIN_JUI . 'syspages/edit/wizard.php';
class juiacc_edit_wizard extends jui_edit_wizard {
//class juiacc_edit_wizard {
    private $_data = array();
    
    public function html_scheme() {
        return $this->htmlRadio('scheme');
    }
    public function html_flags() {
        return $this->htmlCheck('flags');
    }
    
    
    
    
    
    
    private function _html_radio($target) {
        $data = $this->getData($target);
        if (empty($data)) {
            return $this->_getHtmlError();
        }
        
        $items = array();
        foreach ($data as $item) {
            $items[$item] = $item;
        }
        
        $html_out = '<div class="leftalign juiradio">'.NL;
        $form = new Doku_Form(array('id' => 'juiacc_'.$target));
        $form->startFieldset(ucfirst($target));
        $form->addRadioSet($target, $items);
        $form->endFieldset();
       
        

        $html_out .= $this->_htmlForm($form);
        $html_out .= '</div>'.NL;
        return $html_out;
    }
    
    
    
    
    function html_text() {
        global $lang;
        global $conf;

//        $data = $this->getData();
        
        $html_out = '<div class="leftalign juitext">'.NL;
        $form = new Doku_Form(array('id' => 'dw__login'));
//        $form->startFieldset($lang['btn_login']);
        $form->addElement(form_makeTextField('u', ((!$_REQUEST['http_credentials']) ? $_REQUEST['u'] : ''), $lang['user'], 'focus__this', 'block'));
        $form->addElement(form_makePasswordField('p', $lang['pass'], '', 'block'));
        if($conf['rememberme']) {
            $form->addElement(form_makeCheckboxField('r', '1', $lang['remember'], 'remember__me', $class));
        }
//        $form->endFieldset();

        $html_out .= $this->_htmlForm($form);
        $html_out .= '</div>'.NL;
        return $html_out;
    }
    
    function html_radio() {
        // aus tpl_subscribe()
        global $lang;
        global $conf;

        $html_out = '<div class=".leftalign juiradio">'.NL;
        $targets = array(
                '$ID' => '<code class="page">'.'targets-XXXX'.'</code>',
                '$ns' => '<code class="ns">'.'targets-YYYY'.'</code>',
                );
//        $styles = array(
//                'every'  => $lang['subscr_style_every'],
//                'digest' => sprintf($lang['subscr_style_digest'], 10),
//                'list' => sprintf($lang['subscr_style_list'], 12),
//                );
        $styles = array(
                'every'  => 'xxx',
                'digest' => 'yyy',
                'list'   => 'zzz',
                );

        $form = new Doku_Form(array('id' => 'subscribe__form'));
//        $form->startFieldset($lang['subscr_m_subscribe']);
//        $form->addRadioSet('sub_target', $targets);
        $form->startFieldset($lang['subscr_m_receive']);
        $form->addRadioSet('sub_style', $styles);
        $form->endFieldset();
        
        $html_out .= $this->_htmlForm($form);
        $html_out .= '</div>'.NL;
        return $html_out;
    }
    
    function html_combobox() {
        // aus html_recent()
        global $lang;
        global $conf;

        $html_out = '<div class="centeralign">'.NL;
        $form = new Doku_Form(array('id' => 'dw__recent'));
        $form->addElement(form_makeListboxField(
                    'show_changes',
                    array(
                        'pages'      => $lang['pages_changes'],
                        'mediafiles' => $lang['media_changes'],
                        'both'       => $lang['both_changes']),
                    'both',
                    $lang['changes_type'],
                    '','',
                    array('class'=>'quickselect')));
        $form->addElement(form_makeListboxField(
                    'xxxx',
                    array(
                        'pagesXX'      => '__pagesXX',
                        'mediafilesXX' => '__mediafilesXX',
                        'bothXX'       => '__bothXX'),
                    'bothXX',
                    $lang['changes_type'],
                    '','',
                    array('class'=>'quickselect')));
        $form->addElement(form_makeListboxField(
                    'more',
                    array(
                        'aaa'   => '__aaaaa',
                        'sss'   => '__sssss',
                        'ddd'   => '__ddddd',
                        'fff'   => '__fffff',
                        'ggg'   => '__ggggg'),
                    '',
                    '',
                    '','',
                    array('class'=>'quickselect')));
        
        $html_out .= $this->_htmlForm($form);
        $html_out .= '</div>'.NL;
        return $html_out;
    }
    
    function html_check() {
        global $lang;
        global $conf;

        $html_out = '<div class="leftalign juicheck">'.NL;
        $form = new Doku_Form(array('id' => 'dw__login'));
        $class = 'simple';
        $form->addElement(form_makeCheckboxField('a', '1', $lang['remember'], 'remember__me1', $class));
        $form->addElement(form_makeCheckboxField('s', '2', $lang['btn_backtomedia'], 'remember__me2', $class));
        $form->addElement(form_makeCheckboxField('d', '3', $lang['btn_reset'], 'remember__me3', $class));
        $form->addElement(form_makeCheckboxField('f', '4', $lang['btn_recover'], 'remember__me4', $class));
        $form->addElement(form_makeCheckboxField('g', '5', $lang['badlogin'], 'remember__me5', $class));

        $form->addElement(form_makeCheckboxField('ax', '1', 'aaaaa', 'remember__me11', $class));
        $form->addElement(form_makeCheckboxField('sx', '2', 'sssss', 'remember__me21', $class));
        $form->addElement(form_makeCheckboxField('dx', '3', 'ddddd', 'remember__me31', $class));
//        $form->addElement(form_makeCheckboxField('fx', '4', 'fffff', 'remember__me4', $class));
//        $form->addElement(form_makeCheckboxField('gx', '5', 'ggggg', 'remember__me5', $class));

        $html_out .= $this->_htmlForm($form);
        $html_out .= '</div>'.NL;
        return $html_out;
    }
    
    private function _htmlForm(&$form) {
        // Safety check in case the caller forgets.
        $form->endFieldset();
        return $form->getForm();
    }
    public function getData($target) {
        if (empty($this->_data)) {
            $file = dirname(__FILE__) . '/wizard.data.json';
            $this->_data = @json_decode(@file_get_contents($file), true);
        }
        
        $data = $this->_data;
        if (is_array($data[$target])) {
            return $data[$target];
        }

        return array();
    }
    private function _getHtmlError($msg='No data found.') {
        return '<div class="error">'.$msg.'</div>';
    }

    
}
?>
