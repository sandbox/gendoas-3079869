function juiaccOnJqReady(data) {

    if ( !juiwindata.widgetsExists.juiacc ) {
        // there are no accordions on that page
        return;
    }

    juiwindata.oninit = true;
    var accs = $jq('.juiacc-widget', data['context']);
    accs.each(function() {
        var acc = $jq(this);
        var vars = acc.data('juivars') || {};


        var jopts = vars.juiacc || {};
//        var hopts = juiOrNull(vars.head);
//        var copts = juiOrNull(vars.content);
//        var fopts = juiOrNull(vars.fillspace);

        var oopts = widgetOpts(jopts.opts);
        var keep_active = oopts.active;
        oopts.active = false;
                            if (false) {
                                var rszr = juiCreateResizer(this.id);
                                $jq(this).wrap(rszr);
                                var orszr = resizerOpts({});
//                                orszr.disabled = true;
//                                var xx1 = vars.stateX.activeX || -999;
        var rs = vars.state && vars.state.resizer ? vars.state.resizer : {};
        var open = rs.height && isNumeric(rs.height)
                    ? rs.height
                    : orszr.height || acc[0].clientHeight + 100;

                                 rszr = $jq('#'+rszr.id).height(open).resizable(orszr);

//                                rszr = $jq('#'+rszr.id).resizable(orszr);
                                oopts.fillSpace = true;
                                oopts.autoHeight = true;
                            }

        acc.accordion(oopts);
        if (jopts['removeClass']) acc.removeClass(jopts['removeClass'])
        if (jopts['addClass']) acc.addClass(jopts['addClass'])

                            if (false) {
                                var height = { closed: acc[0].clientHeight,
                                               open: open}
                                rszr.data('height', { closed: acc[0].clientHeight,
                                               open: open});
                                var xx = rszr.data('height');
                           }

        oopts.active = keep_active;  // restore
        var active = vars.state && isNumeric(vars.state.active)
                        ? vars.state.active
                        : oopts.active || -1;
        if (active >= 0) {
            $jq(this).accordion("activate", active);

//            $jq('#'+rszr.id).resizable('option', 'disabled', false);
        }


//    if (state) {
//        attr = document.createAttribute('style')
//        attr.nodeValue = 'height:' + state.height + 'px';
//        elem.setAttributeNode(attr);
//    }
//                    $jq(this).accordion("resize");

    });
    juiwindata.oninit = false;



    //
    // functions
    //
    function widgetOpts( opts ) {
        if (!opts) opts = {};
        $jq.extend(opts, {
            changestartX: function (event, ui) {
                                var id1 = this['id'];
                                var id2 = this.id;
                    var state = $jq.data(this, 'juivars').state;
                    if (!juiwindata.oninit && state) {
                        state.active = $jq(this).find(ui.options.header).index(ui.newHeader);
                        $jui.ajax.storeState($jui.widget.getCleanID(this['id']), state)
                    }

                    // may be ajax call is needed
                    $jui.ajax.parseContent($jui.cross.getEventTarget(event), ui.newContent);

//                    // resizer
////                    if (state && state.active && (var rszr = $jq('#'+this['id']+'-resizer'))) {
//                    var rszr = juiGetResizerJq(this['id']);
//                    if (state && state.active && rszr) {
//                        var hc = rszr.data('height');
//                        rszr.height(state.active >= 0 ? hc.open : hc.closed);
////                        $jq(this).accordion('resize');
//                        var c = $jq('.juiacc-content', this);
//                        $jq('.juiacc-content', this).each(function() {
//                            var cx = $jq(this);
//                            cx.height(hc.open - hc.closed);
//                            var xx =0;
//                        });
//                    }
            }
        });
        return opts;
    }

    function resizerOpts( opts ) {
        if (!opts) opts = {};
        $jq.extend(opts, {
            autoHide: false,
            handlesX: 's',
            resizeX: function(event, ui){
//                    $jq(this).accordion('resize');
                  },
            stopX: function(event, ui) {
                    var state = $jq(this).data('juivars').state;
                    if (!juiwindata.oninit && state) {
                        state.resizer = { height: ui.size.height };
                        $jui.ajax.storeState($jui.widget.getCleanID(id), state)
                        $jq(this).accordion('resize');
                    }
                }
        });
        return opts;
    }

}

