<?php
/**
 * DokuWiki Plugin juiacc (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Albin Spreizer <albin@gendoas.de>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_TAB')) define('DOKU_TAB', "\t");
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');

require_once DOKU_PLUGIN.'juiwidget/syntax.php';

class syntax_plugin_juiacc extends syntax_plugin_juiwidget {

    function getType() { return 'substition';}
    function getPType() { return 'block';}
    function getSort() { return 310; }


    public function connectTo($mode) {
        $this->Lexer->addSpecialPattern('<juiacc.+?</juiacc>',$mode,'plugin_juiacc');
    }

    public function handle($match, $state, $pos, $handler){

                            $testVars = $this->testVars();


        // if ( $this->getIsNesting() ) >>> don't abort, data are needed (instructions) !!!

        // options for that widget
        $plugin = $this->getPluginName();
        $data = array('match' => $match);
        list($opts, $content) = explode('>', substr($match, 7, -9), 2);
        // options for the widget itself
        $data['opts'][$plugin] = $opts;
        // more options
        $content = $this->extractOptions($content, $data['opts']);


        // sections
        $pattern = '&<acc-section.+?</acc-section>&is';
        preg_match_all($pattern, $content, $sections);
        $data_sects = array();
        $idx = 0;
        foreach($sections[0] as $section){
            list($opts, $content) = explode('>', substr($section, 12, -14), 2);
            // options for that section itself
            $data_sects[++$idx]['opts']['head'] = $opts;
            // more options
            $data_sects[$idx]['content'] = $this->extractOptions($content, $data_sects[$idx]['opts']);
        }
        $data['sections'] = $data_sects;

                    $msg = "match=$match";
//                    juiDbgMsg("..........handle:: $msg", -1);
//                                $match = $data['match'];
//                                msg('render:: ' . $match);

        return $data;
    }

    public function render($mode, $renderer, $data) {

        // for ajax prevent caching
        $this->ajaxSetCache($renderer);     //JUIFIX::   ??? 1.mode, 2.delete  ???
        
        if ($mode != 'metadata' && $mode != 'xhtml') {
            return false;
        }
        
        if ($this->isPreview() && mode == 'metadata') {
            return false;
        }

        // nesting not allowed,
        if ($this->getIsNesting() && $mode == 'xhtml') {
            // show message-page
            $renderer->doc .= $this->showMsgHtml(array('msgkey' => 'no_nesting'));
            return true;
        }

        // section needed
        if ( !count($data['sections']) ) {
            if ($mode == 'metadata') {
                juiDbgMsg('acc-sections required.');
            }
            return false;
        }


        // parse options, widget and its sections
        $data['opts'] = $this->parseOptionString($data['opts'], 'widget');
        foreach ($data['sections'] as &$section) {
            $section['opts'] = $this->parseOptionString($section['opts'], 'section');
        }

        $plugin = $this->getPluginName();
        $uid = $data['opts'][$plugin]['uid'];
        $juiid = $this->getWidgetUID($uid, $data['match'], $plugin);
        $data['plugin'] = $plugin;
        $data['juiid'] = $juiid;


        if ($mode == 'metadata') {
            return $this->prepareJuiVars($juiid, $data, $renderer);
        } else /*($mode == 'xhtml')*/ {
            if ($this->isPreview() || $this->isSyspage()) {
                $this->prepareJuiVars($juiid, $data, $renderer);
            }
            return $this->_renderXhtml($mode, $renderer, $data);
        }
    }
    
    private function _renderXhtml($mode, &$renderer, &$data) {
        global $ID;

        if($mode != 'xhtml') {
            return false;
        }

        $plugin = $data['plugin'];
        $juiid  = $data['juiid'];
        $scheme = $data['opts'][$plugin]['scheme'];

//                    $msg = "mode=$mode, juiid= $juiid";
//                    juiDbgMsg("..........render:: $msg", 0);

        $doc .= juiDbgMsgHtml("\n<!-- accordion-start -->\n");
        $p = array();
        $p['id']    = $juiid;
        $p['class'] = $this->getWidgetClasses($scheme);
        $p['page']  = $ID;
        $att = buildAttributes($p);
        $doc .= "<div $att>\n";


        // sections
        $c_dflt = $this->getLang('head_caption');
        $c_dflt = $this->headCaption($data['opts'], $c_dflt);
        foreach( $data['sections'] as $idx => $section ) {
            $doc .= juiDbgMsgHtml(DOKU_TAB . "<!-- acc-section$idx-start -->\n");
            $caption = $this->headCaption($section['opts'], "$c_dflt-$idx");
            $title   = $this->headTitle($section['opts'], $caption);

            // section-head
            $p = array();
            $p['id']    = $this->getExtUID($juiid, $idx, '-h');
            $p['class'] = $plugin . '-header';
//            $p['title'] = $title;
            $att = buildAttributes($p);
                // JUIFI-X:: $mnu->ui-icon-wrench
                $mnu = "\n" . DOKU_TAB
                        .'<div id="' . $this->getExtUID($juiid, $idx, '-mnu') . '" '
                        .'class="jui-icon-left jui-tbwrench jui-abs-se ui-icon-none"'
                        .'></div>';
//                $mnu = '';
            $doc .= DOKU_TAB . "<div $att>" . '<a href="#" ' . ">$caption</a>$mnu</div>\n";

            // section-content
            $p = array();
            $p['id']    = $this->getExtUID($juiid, $idx, '-c');;
            $p['class'] = CLS_JUICONTENT . $plugin . '-content';
            $att = buildAttributes($p);
            $doc .= DOKU_TAB . "<div $att>\n";

            // parse the content
//                    juiDbgMsg("..........renderJuiContent.section=$caption, juiid=$juiid", 2);
            $html = $this->renderJuiContent($section['content'], $juiid);
            $doc .= DOKU_TAB . DOKU_TAB . $html . "\n";

            // end section
            $doc .= DOKU_TAB . "</div>" . juiDbgMsgHtml("<!-- acc-section$idx-end -->\n");
        }
//                $mnu = '<div style="display:none">asdf<div>'
                $mnu ='';
        $doc .= $mnu . '</div>';
        $doc .= juiDbgMsgHtml("<!-- accordion-end -->\n");

        $renderer->doc .= $doc;
        return true;
    }



    /*
     * Testfunktionen
     */
    private function testGetContent($file) {
        //return file_get_contents(DOKU_PLUGIN_JUIACC . '___test/' . $file);
    }

}

// vim:ts=4:sw=4:et:
